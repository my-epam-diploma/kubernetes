# Kubernetes
WIP
Manifests and helm templates

# Usefull commands

## Cluster provisioning and managing in AWS
Provide cluster
```bash
eksctl create cluster --name epam-diploma --nodegroup-name diploma-ng --node-type t3.medium --nodes-min=1 --nodes-max=4
```
Scale cluster
```
eksctl scale nodegroup --cluster=epam-diploma --nodes=4 diploma-ng
```
Delete cluster
```bash
eksctl delete cluster --name epam-diploma
```
## Preparation for HTTPS
Install nginx-ingress
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/aws/deploy.yaml
```
Install cert-manager
```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.yaml
```
Renew sonarqube certificate for HTTPS
```bash
kubectl cert-manager renew -n sonarqube
```
## Install with helm
Install production version with Helm
```bash
helm install prod-diploma-app ./helm/diploma-app --values ./diploma-app/prod.values.yaml
```
Install staging version with Helm
```bash
helm install staging-diploma-app ./helm/diploma-app --values ./diploma-app/staging.values.yaml
```
## Dry run before apply
```bash
kubectl apply -f ./cert-manager/ --dry-run=client
```

## Reach kubernetes-dashboard
```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```
kube-proxy address
```bash
http://localhost:8001/api/v1/namespaces/default/services/https:kubernetes-dashboard:https/proxy/#/login
```

## Artificial CPU load
```bash
sha1sum /dev/zero
```